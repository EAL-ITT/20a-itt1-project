---
Week: 43
tags:
- ADC
- documentation
- fablab workshops
---


# Exercises for ww43

## Exercise 0 - Self assesment part 1

### Information

This is an individual exercise. 
It is a tool to help you asses your own knowledge regarding todays subjects in project.
It is done by taking a quiz in the morning and a quiz in the afternoon, the quiz has the same questions and will show you the knowledge you have gained in todays project lectures.

### Instructions

1. Complete the quiz at [https://forms.gle/6rZtAqTao6xx6fkr5](https://forms.gle/6rZtAqTao6xx6fkr5)
2. Note your score and which questions you can improve on

## Exercise 1 - Reading voltage with ADC

### Information

This is a team exercise

The Raspberry Pi does not have an analog to digital converter (ADC) and can only read digital (binary) signals from it's GPIO pins.
In this exercise you will learn how to add this capability ro your RPi by interfacing a 10bit ADC to it.  
You will test that you can read analog signals by reading a varying voltage from a potentiometer.  

## Instructions

Follow the guide at [https://eal-itt.gitlab.io/discover-iot/rpi/adc](https://eal-itt.gitlab.io/discover-iot/rpi/adc)

Share your learned lesson with your team and ask one or more team members to follow your guide, correct any errors in the guide if needed.

## Exercise 2 - Fablab workshops

2 persons from each team according to plan, goes to fablab workshops.

Fablab workshops start at 8:15 + 12:15.

## Exercise 3 - Self assesment part 2

### Information

This is an individual exercise. 
It is a tool to help you asses your own knowledge regarding todays subjects in project.
It is done by taking a quiz in the morning and a quiz in the afternoon, the quiz has the same questions and will show you the knowledge you have gained in todays project lectures.
It is required to use your school email to access the quiz

### Instructions

1. Complete the quiz at [https://forms.gle/gjqA9KkQ8u1Prw3V6](https://forms.gle/gjqA9KkQ8u1Prw3V6)
2. Note your score and your improvement, compared to exercise 0 results from the beginning of the day.
