---
Week: 47
tags:
- documentation
- System design
---


# Exercises for ww47

## Exercise 0 - Project plan presentations

You are to make a live presentation where you convey your idea for to your classmates.

1. Decide on group how you want to present your project in 5-10 minutes

2. Decide on how to receive feedback

    Suggestions are to use issues with e.g. a `feature request` label

2. Create slides (or equivalent)

3. Put docs on gitlab for future reference
