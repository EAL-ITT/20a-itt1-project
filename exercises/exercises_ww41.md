---
Week: 41
tags:
- enclosure
- documentation
- planning
---


# Exercises for ww41

## Exercise 0 - Self assesment part 1

### Information

This is an individual exercise. 
It is a tool to help you asses your own knowledge regarding todays subjects in project.
It is done by taking a quiz in the morning and a quiz in the afternoon, the quiz has the same questions and will show you the knowledge you have gained in todays project lectures.

### Instructions

1. Complete the quiz at [https://forms.gle/WiK9Zp2WxUXuZvtS8](https://forms.gle/WiK9Zp2WxUXuZvtS8)
2. Note your score and which questions you can improve on

## Exercise 1 - Design of enclosure

### Information

In preparation of the session in fablab after the autumn break, you are to start considering how you want to protect and present you system.


### Instructions

1. Update/create a high level block diagram of the system

2. From the block diagram, make a list of the parts of your system and note the size of each part.

    This is the raspberry pi, LED/BTN board and perhaps other devices.

2. Make a list of ingoing, outgoing and internal connections.

3. Make a list of connectors, LEDs, buttons and other things that need to be accessible or visible from outside the enclosure.

4. Make a sketch of the enclosure from multiple angles (includin inside)

    Include height, width and distances and label holes appropriately

5. Upload to gitlab


## Exercise 2 - Documentation

### Information

In week 45 we will conclude part 1 of the project with an exercise where you recreate another team's system from their documentation.
If the team that are recreating your system can't fin dyour documentation they will create a `bug` report in your issue board. You then have to support them immediately on how to proceed.  
Keep in mind that your team will also be recreating while doing support.  


Therefore, it will be a good strategy to begin organizing your documentation as step by step guides on how to recreate your system. This will keep support issues on a low level. 

### Instructions

1. In your team agree on a structure for documentation, this can be as single `.md` documents for each part of your system, contained in a folder.  
Or you can use the wiki functionality of gitlab.  
The documentation for gitlab's wiki can be found here [https://docs.gitlab.com/ee/user/project/wiki/](https://docs.gitlab.com/ee/user/project/wiki/)
2. Make seperate tasks/issues in your gitlab issue board for each piece of documentation you need to rewrite as a step by step guide. Examples are: `Thingspeak setup`, `Rasperry pi ssh keys setup` etc.
3. In the `readme.md` file in your gitlab project write a section that explains where to find your documentation (with links) and how it is organized.
4. In the future remember to document new parts of your system as step by step guides, with recreation in mind.

## Exercise 3 - Fablab workshops planning

### Information

In week 43 and 44 we have planned workshops for you in Fablab.
There will be 2 workshops in week 43 and 2 workshops in week 44.
Each workshop has a capacity of 16 people and we need to know whp is going when.

### Instructions

1. Decide in your team who goes to which workshop - Only 2 persons from each team is allowed at each workshop
2. Write your full name and school email address on hack.md [https://hackmd.io/@nisi/ByDrxZfIv/edit](https://hackmd.io/@nisi/ByDrxZfIv/edit) (*sign in is required*)
3. Make an issue in your gitlab project for each workshop, include date, time and names - the purpose of this is planning of work within the team.

## Exercise 4 - Self assesment part 2

### Information

This is an individual exercise. 
It is a tool to help you asses your own knowledge regarding todays subjects in project.
It is done by taking a quiz in the morning and a quiz in the afternoon, the quiz has the same questions and will show you the knowledge you have gained in todays project lectures.
It is required to use your school email to access the quiz

### Instructions

1. Complete the quiz at [https://forms.gle/CkVuy1qk5sBRojoR9](https://forms.gle/CkVuy1qk5sBRojoR9)
2. Note your score and your improvement, compared to exercise 0 results from the beginning of the day.
