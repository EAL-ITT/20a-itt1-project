---
Week: 38
tags:
- Electronics
- SSH
- Team contract
---


# Exercises for ww38

## Exercise 0 - How to ensure stuff is working

### Information

Throughout the 1st semester project you will be developing stuff, but what are the most efficient ways of wnsuring that it actually works ?

### Exercise instructions

1. Read the article at the companion site [https://eal-itt.gitlab.io/discover-iot/project_management/stuff_working](https://eal-itt.gitlab.io/discover-iot/project_management/stuff_working)
2. Create file in your gitlab project called `log_ww38.md` and update it with experiences from each of the week 38 exercises.
3. Format the markdown file using the markdown cheatsheet located at [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) 

## Exercise 1 - led/button module build

### Information

The most simple human interface to an IoT system are physical buttons for input to the system and LED's for output from the system.

In this exercise you will build a small PCB that has 2 buttons and 2 LED's. It also has a connector to connect to your raspberry pi's gpio pins.

### Exercise instructions

Because of covid-19 restrictions only 2 students from each team are allowed in electronics lab (e-lab room A1.18) at the time. This means that a total number of students in electronic lab may not exceed 16 people at the same time. Please coordinate this in your team.

Electronics lab is reserved for you from 11:00 - 16:15.

Your student card should allow access to electronics lab, otherwise ask a lecturer for access.

Before going to e-lab, read the instructions on how to build the module from the companion site [https://eal-itt.gitlab.io/discover-iot/electronics/led_btn_module](https://eal-itt.gitlab.io/discover-iot/electronics/led_btn_module)

## Exercise 2 - SSH setup on Raspberry pi

### Information

In this exercise you will learn how to setup SSH on your raspberry pi.

### Exercise instructions

Go to [https://eal-itt.gitlab.io/discover-iot/misc/ssh](https://eal-itt.gitlab.io/discover-iot/misc/ssh) for detailed instructions.
 
## Exercise 3 - Team contract on gitlab

### Information

The team contract you created in week 37 needs to be included in your gitlab project as a markdown file (.md)

### Exercise instructions

1. Create a .md file named `team_contract.md` in your gitlab project
2. Pull the changes to your local computer
3. Open `team_contract.md` in a text editor 
4. Copy the contents of your team contract form ww37 in to `team_contract.md`
5. Format `team_contract.md` with headlines etc. using the markdown cheatsheet located at [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) 
