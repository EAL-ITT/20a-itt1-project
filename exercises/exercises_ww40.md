---
Week: 40
tags:
- video presentation
- python
- temperature sensor
---


# Exercises for ww40

## Exercise 0 - Proof of concept video

### Information

Proof of concept (POC) ended in week 39 and we would like to see what you accomplished.

You need to produce a video ~3 minutes long, that includes a walkthrough and demo of your POC system. The video should be uploaded to youtube or similar and shared with teachers and the other project groups.
We are not experts in video editing software, however a quick google search revealed [https://www.openshot.org/](https://www.openshot.org/) that works for mac, linux and windows.
Something like [https://obsproject.com/](https://obsproject.com/) might also work for you.

## Instructions

1. Setup your POC system
2. Make a video that explains the moving parts and a small demo of the system
3. Share your video with teachers and other groups on [hackmd](https://hackmd.io/@nisi/ryEOhtVHw/edit) (requires hack.md account)

The deadline for submission of the video is at 2020-09-28 16:15 AM.

## Exercise 1 - DS18B20 temperature sensor

### Information

In the 37 sensors kit you have a temperature sensor called DS18B20. The temperature sensor uses it's own one wire protocol and can be connected and read from the Raspberry Pi.

The temperature sensor can out temperatures in Celcius, Fahrenheit and Kelvin.

## Instructions

Follow the guide at [https://eal-itt.gitlab.io/discover-iot/rpi/temp-sensor](https://eal-itt.gitlab.io/discover-iot/rpi/temp-sensor)

Document your work on gitlab, with a step by step guide on how to set up the temperature sensor.  
Share your learned lesson with your team and ask one or more team members to follow your guide, correct any errors in the guide if needed.

## Exercise 2 - Combining parts

### Information

We have a lot disjoint parts, and we are to combine them now.

The parts

* In exercise 1, you have temperature readings on the raspberry using python
* In week 39 exercise 1, you connected your LED/button to the raspberry using python
* In week 39 exercise 0, you connected to thingspeak using python
* (Bonus) In programming you have looked at flow charts. See [programming week 39](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/exercises_ww39) Exercises 0 and 1.

These are to be combined now.

Requirements for the system

* When button 1 (reset button) is clicked, connection to thingspeak is tested, and fail/success is saved.
* Diode 1 (status diode) must be lit when there is connection to thingspeak, and blink on error.
* When button 2 (read button) is clicked, the temperature is read and send to thingspeak.
* Diode 2 (busy diode) must be lit from the button is pressed to when data exchange wit thingspeak is done.

Note that may be distributed between teammember and the code combined gitlab.

## Instructions

1. Create flow diagrams for the following functions

  * `update_status( new_status )`: function that updates the status LED based on the value of `new_status`.
  * `reset_button_pressed()`: function that connects to thingspeak and return `false` on error and `true` on success.
  * `update_busy( new_state )`: function that updates the busy LED based on the parameter `new_state`.

2. Upload docs to gitlab

2. Implement the functions and test that they work.

    If you are rusty in the domain of python functions, syntax help is on [w3schools](https://www.w3schools.com/python/python_functions.asp)

3. Create a flow diagram using the functions defined above.

    In this iteration, you will use `while True:` to do busy waits.

    Also, use [`button.is_pressed()`](https://gpiozero.readthedocs.io/en/stable/api_input.html#gpiozero.Button.is_pressed). Blinking is implemented using [`led.blink()`](https://gpiozero.readthedocs.io/en/stable/api_output.html#gpiozero.LED.blink) with `background` set to `true`.

2. Upload docs to gitlab

4. Implement and test

5. Upload code to gitlab.
