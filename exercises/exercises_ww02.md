---
Week: 02
tags:
- Presentations
---


# Exercises for ww02

### Exercise 0 - Project presentations

You will be presenting on class. Participation in the presentations is mandatory to pass OLA12.

Suggestions are powerpoint for overview combined with live demo of using the system.

Each team has 10 minutes for the presentation followed by 5 minutes of questions from class and lecturers

It is important that every team member presents a part of the project (ie. no one from the team will be passive during the presentation)

**Presentation timetable**

| Time  | Team |
| :---- | :----  |
| 13:00 | Team B1 | 
| 13:15 | Team A1 |
| 13:30 | Team B2 |
| 13:45 | Team A2 |
| 14:00 | Break |
| 14:15 | Team B3 | 
| 14:30 | Team A3 |
| 14:45 | Team B4 |
| 15:00 | Team A4 |