---
title: '20A ITT1 Project'
subtitle: 'OLA12 - tentative'
filename: '20A_ITT1_PROJECT_OLA_12'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-11-19
email: 'nisi@ucl.dk'
left-header: \today
right-header: OLA12
skip-toc: false
---

# Introduction

This obligatory learning activity concludes the 1st semester project.
The conclusion consists of a project presentation and a finalized project documented on gitlab.

In ww46, at the project part 2 "Proof of concept" phase, you decided on goals for your project as well as a use case.

The presentation will have your intented users as audience and the purpose for them to understand what your system does and how to start using it. Presentation duration is 15 minutes.

See it, as if you were giving an initial introduction to your system, to a group of users.

# Instructions

## Presentation

1. In the group, discuss which features and you will be presenting to the users

2. Decide on appropriate presentation technique for each feature

    You will be presenting on class.

    Suggestions are powerpoint for overview combined with live demo of using the system.

3. Create the presentation and write a script/framework for the presentation.

## Hand-in 

The hand-in is a pdf handed in on wiseflow. It needs to contain the following:

* Frontpage with team member names, semester, course, school logo, date and character count
* Numbered table of contents with page numbers

* A description of your project.
    * Finalized project plan
    * Explain how you structered team work (meetings, workload, collaboration, issue management etc.)
    * Use case - what problem is the system trying to solve ?
    * Block diagram
    * Recreation manual
    * Documentation of system tests
    * User manual
    * Picture of the final product including casing
    * Link to project gitlab page (a webpage)
    * Suggestions on how to proceed development to improve or expand the system (ie. what parts didn't work and what are the challenges?)

Large diagrams etc. needs to be put in the appendix. 
We expect 5-10 pages + appendix. Note that this is an individual hand-in.

Please link to code on Gitlab instead of including large code blocks, explain them with flow charts and maybe snippets of essential code (1-3 lines) if needed.  


# Follow up 

After hand-in, the lecturers will review the handed in documents.  

Results are communicated through wiseflow.