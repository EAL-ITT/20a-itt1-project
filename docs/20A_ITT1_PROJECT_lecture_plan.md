---
title: '20A ITT1 PROJECT'
subtitle: 'Lecture plan'
filename: '20A_ITT1_PROJECT_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>', 'Morten Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-11
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 20A
---


# Lecture plan - tentative

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait20, 20A
* Name of lecturer and date of filling in form: NISI, ILES 2020-06-11
* Title of the course, module or project and ECTS: Semester project, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Teacher |  Week | Content |
| :---: | :---: | :--- |
|  NISI/MON | 37 | P1P1 (POC) Exercises: gitlab setup, pre mortem, project plan, Riot/element |
|  NISI/MON | 38 | P1P1 (POC) Exercises: How to ensure stuff is working, led/button module build, SSH setup RPi, Team contract on gitlab |
|  NISI/MON | 39 | P1P1 (POC) Exercises: Thingspeak connect, led/button module test, SSH keys agent forwarding, Contributing code on gitlab |
|  NISI/MON | 40 | P1P2 (Consolidation) Exercises: Proof of concept video, DS18B20 temperature sensor, Combining parts |
|  NISI/MON | 41 | P1P2 (Consolidation) Exercises: Self assesment, Design of enclosure, Documentation, Fablab workshops planning |
|  NISI/MON | 42 | No lectures |
|  NISI/MON | 43 | P1P2 (Consolidation) Exercises: Self assesment, Reading voltage with ADC, Fablab workshops |
|  NISI/MON | 44 | P1P3 (Make it useful) Exercises: Minimum system, Fablab workshops, Motor |
|  NISI/MON | 45 | P1P3 (Make it useful) OLA11: Recreate other teams system from documentation |
|  NISI/MON | 46 | P2P1 (POC) Exercises: Part 2 design and plan, Feedback issue |
|  NISI/MON | 47 | P2P1 (POC) Exercises: Project plan presentations |
|  NISI/MON | 48 | P2P1 (POC) Exercises: POC wrapup, Feedback |
|  NISI/MON | 49 | P2P2 (Consolidation) Exercises: RPi images |
|  NISI/ILES | 50 | P2P2 (Consolidation) Exercises: Gitlab pages |
|  NISI/ILES | 51 | P2P2 (Consolidation) Exercises: User manual, prepare presentations and hand-in |
|  NISI/ILES | 01 | P2P3 (Make it useful) Exercises: Catch up |
|  NISI/ILES | 02 | P2P3 (Make it useful) OLA12: Product presentations and demo |

**Explanation of phases**

Proof of concept: Minimal functionality throughout the entire system (ie. end to end communication)
Consolidation: Full implementation of features
Make it useful for the user: Documentation, User manual, recreation manual etc.

# General info about the course, module or project

The purpose of the course is to ....

## The student’s learning outcome

At the end of the course, the student .....

The student possesses knowledge and understanding of
* a project management model relevant to development projects in IT
* a systems development method relevant to development projects in IT.

Skills - The student is able to
* apply a project management method to development projects in IT
* apply knowledge, methods and tools to the design, development and testing of products or systems

Competencies - The student is able to
* manage the interaction between given technologies in integrated solutions
* carry out planning and quality management of the student’s own technical tasks
* in a structured setting, acquire new knowledge, skills and competencies in specific sub-areas


## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

The project will be divided into blocks that build upon each other. The idea is to create a system that works from sensor to presentation in the cloud while the students practice project management and learn operational skills.

## Equipment

None specified at this time as it is dependent on the topic chosen.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
The project includes 1 obligatory learning activity.

See semester description for details on compulsory elements.

## Other general information
None at this time.
