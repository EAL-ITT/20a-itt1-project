---
Week: 51
Content: P2P2 Consolidation 3/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 51

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* User manual created

### Learning goals
* User manual
  * The student knows the purpose of a user manual
  * The student can write a useful user manual
  * The student can write a comprehensive user manual

## Deliverables

* Optional status meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Feedback on an accomplished task
    2. Help needed
    3. AOB

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. AOB

## Schedule

### Monday  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Status meetings - Book a time if you need help or feedback (optional) |
| 9:30 | Hands on time | 
| 12:15 | Lunch |
| 16:15 | end of the day |


## Hands-on time

### Exercise 0 - User manual

...

### Exercise 1 - Prepare presentations and hand-in (OLA12)

...

## Comments

You have recived a link to a survey in your school email, we need your feedback, please complete it.