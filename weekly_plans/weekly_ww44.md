---
Week: 44
Content:  P1P3 Make it useful for the user 1/2
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 44

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Minimum system implemented
* Recreation guide for minimum system created

### Learning goals
* Minimum system
  * Level 1: Students can implement minimum system requirements with one sensor + recreation guide
  * Level 2: Students can implement minimum system requirements with two sensors + recreation guide
  * Level 3: Students can implement minimum system requirements with all functionality + recreation guide

## Deliverables

* 10 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Group morning meetings |
| 10:00 | Teacher meetings - A2 and B2 starts |
| 10:00 | You work on exercises |
| 12:15 | Lunch |
| 10:00 | You work on exercises | 
| 14:00 | Q&A online at [https://meet.jit.si/20a-itt1-project](https://meet.jit.si/20a-itt1-project) |
| 16:15 | end of the day |


## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - Minimum system

...

### Exercise 1 - Fablab workshops

2 persons from each team goes, according to plan, to fablab workshops.

Fablab workshops start at 8:15 + 12:15.

### Exercise 2 - Motor

## Comments

