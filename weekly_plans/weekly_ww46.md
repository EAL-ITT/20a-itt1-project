---
Week: 46
Content: P2P1 Proof of concept 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 46

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Participate in datacenter virtual tour
* Project part 2 described

### Learning goals
* Part 2 design and plan
  * The student can, in a team setting, define a use case
  * The student can, in a team setting, create an overview of the system
  * The student can create milestones and issues describing the work to be carried out in part 2

## Deliverables

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | datacenter virtual tour |
| 11:00'ish | Hands-on time |
| 12:15 | Lunch |
| 16:15 | end of the day |


## Hands-on time

### Exercise 0 - Part 2 design and plan

...

### Exercise 1 - Feedback issue

...

## Comments

* Morten only available on Element  
* Datacenter tour - Be ready and seated in class 8:50
