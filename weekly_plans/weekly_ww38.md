---
Week: 38
Content:  P1P1 Proof of concept 2/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 38

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* How it works logs created
* Led/button module soldered and tested 
* SSH access to RPi

### Learning goals
* SSH access to RPi
  * Level 1: Students know in brief what SSH is
  * Level 2: Students can set up SSH access on a Raspberry pi
  * Level 3: Students can set up passwordless SSH access on a Raspberry pi

## Deliverables

* Test plan included in gitlab project (.md file format)
* Led/button module build documented in gitlab project
* SSH access guide documented in gitlab project

* 15 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)  
If you have disagreements in the team, this is normal, just make sure to inform us.
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)  
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  Make sure to read about using gitlab issues at [Discover IoT](https://eal-itt.gitlab.io/discover-iot/project_management/tasks_issues)
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Any other business (AOB)

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 8:30 | Team morning meetings |
|| Plan this in your team, deadline is Monday 9:00 |
| 9:00 | Introduction |
| 10:00| International Community - external lecturer |
| 11:00 | Teacher meetings - remember to book a time slot |
| 11:00 | You work on exercises (remember lunch 12:15) |
| 13:00 | Study start test - Room A0.29 | 
| 14:15 | Q&A session (jitsi) |
| 14:30'ish | You work on exercises |
| 16:15 | end of the day |


## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - How to ensure stuff is working

...

### Exercise 1 - led/button module build

...

### Exercise 2 - SSH setup on Raspberry pi

...

### Exercise 3 - Team contract on gitlab

...

## Comments

